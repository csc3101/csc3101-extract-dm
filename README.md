# Usage

```./extract.sh path-to-zip-provided-by-moodle outputdir command```

where `command` is the one of the generation step (just type `./extract.sh` to see these steps) or `all`.
At the end of the extraction, you should find the sources in `outputdir/src`. You just have to import
this folder in your prefered code editor.

