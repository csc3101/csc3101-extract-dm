#! /bin/bash

if [ $# -ne 3 ]; then
    echo "Usage: $0 zip rootdir cmd"
    echo "  where cmd is:"
    echo "    - clean: clean everything"
    echo "    - prepare: prepare"
    echo "    - extract: extract the internal archives"
    echo "    - gather: gather the extracted files"
    echo "    - decompile: decompile the missing java files from the class files"
    echo "    - repackage: recreate the packages"
    echo "    - all: clean and then execute all the command"
    exit 1
fi

zip="$1"
rootdir="$2"
cmd="$3"

log() {
    echo "==== $@"
}

llog() {
    echo "---- $@"
}

error() {
    echo "Fatal: $@"
    exit 1
}

clean() {
    log "cleaning"
    rm -rf "$rootdir/base"
}

prepare() {
    log "preparing"
    mkdir -p "$rootdir/base"
    llog "extracting \"$zip\" in \"$rootdir/base\""
    unzip -u -q "$zip" -d "$rootdir/base"
}

extract_single() {
    f=$(basename "$1")
    from=$(pwd)/$1
    to=$(dirname "$1" | sed -e "s|$rootdir/base|$rootdir/extracted|")
    cmd="$2"
    llog "extracting '$f' with '$cmd' in '$to'"
    mkdir -p "$to"; cd "$to" && $cmd "$from" >/dev/null; cd - >/dev/null
}

getext() {
    prev=""
    ext=""
    IFS='.'
    for x in $1; do
        prev="$ext"
        ext="$x"
    done

    if [ "$prev" == "tar" ]; then
        echo "tar.$ext"
    else
        echo "$ext"
    fi
}

extract() {
    log "extracting"
    find "$rootdir/base" -type f |
        while read f; do
            base=$(basename "$f")
            ext=$(getext "$base")
            case "$ext" in
                java|class|pdf|jpg|odt|docx|doc|rtf|txt) ;;
                "Document 1 sans titre") ;;
                jar|zip) extract_single "$f" "unzip -u";;
                rar) extract_single "$f" "unrar x -y";;
                tar|tar.*|7z)  extract_single "$f" "tar xf";;
                *) error "unable to handle extension '$ext' in '$f'";;
            esac
        done
}

gather_from() {
    log "gathering $1"
    find "$1" -type d -depth 1 |
        while read base; do
            dest=$rootdir/gather/$(echo "$base" | sed -e "s|$1/\([^_]*\)_.*|\1|" | sed -e 's|[ -]|_|g')
            mkdir -p "$dest/classes"
            find "$base" -iname "*.java" -exec cp {} "$dest"/ \;
            find "$base" -iname "*.class" -exec cp {} "$dest"/classes \;
        done
}

gather() {
    gather_from "$rootdir/base"
    gather_from "$rootdir/extracted"
}

decompile() {
    log "decompiling missing java files"
    find "$rootdir/gather" -type d -depth 1 |
        while read d; do
            if   find "$d" -iname "*java" -exec false {} + &&
                    ! find "$d" -iname "*class" -exec false {} +; then
                u=$(basename "$d")
                llog "decompile for $u"
                mkdir -p "$d/generated"
                find "$d" -iname "*.class" -not -name "module-info.class" |
                    while read f; do
                        fname=$(basename ${f%.*})
                        llog "  processing $fname"
                        java -jar jd-core-cli.jar "$f" "$d/generated/$fname.java"
                    done
            fi
        done
}

repackage() {
    log "repackaging"
    find "$rootdir/gather" -type d -depth 1 -iname "*" |
        while read d; do
            llog "repackaging $d"
            find "$d" -iname "*.java" -not -name "module-info.java" |
                while read f; do
                    fname=$(basename "$f")
                    dir=$(echo $(dirname "$f") | sed -e "s|$rootdir/gather/||")
                    op=$(cat "$f" | grep package | sed -e 's|.*package \(.*\);.*|\1|' | sed -e 's|\.|/|g')

                    if [ -n "$op" ]; then dir="$dir/$op"; fi

                    package=$(echo "$dir" | sed -e "s|/|\.|g")
#                    echo "$f - $dir - $package - $fname"
                    
                    mkdir -p "$rootdir/src/$dir"

                    ( if [ -z "$op" ]; then echo "package $package;"; echo; fi
                      cat "$f" | gsed -e "s|.*package \(.*\);.*|package $package;|" ) > "$rootdir/src/$dir/$fname"
                done 2>&1
        done
}

all() {
    clean
    prepare
    extract
    gather
    decompile
    repackage
}

case "$cmd" in
    clean|prepare|extract|gather|decompile|repackage|all)   ;;
    *) error "unknow command '$cmd'"
esac

"$cmd"

exit 0
